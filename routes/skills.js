var express = require('express');
var router = express.Router();
const controller = require("../controllers/skills");
const checkPermissions = require('../accessMiddleware');

router.post('/', checkPermissions(['write']),controller.create);

router.get('/', checkPermissions(['read']),controller.list);

router.get('/:id', checkPermissions(['read']),controller.index);

router.put('/:id', checkPermissions(['write']),controller.replace);

router.patch('/:id', checkPermissions(['write']),controller.update);

router.delete('/:id', checkPermissions(['delete']),controller.destroy);


module.exports = router;