const jwt = require('jsonwebtoken');
const config = require('config');

function checkPermissions(allowedPermissions) {
    const jwtkey = config.get('secret.key');
    return (req, res, next) => {
        // Obtén los permisos del usuario desde el token JWT
        const cad = req.headers.authorization;
        const token = cad.substring(7);
        const decoded = jwt.verify(token, jwtKey);
        permissions = decoded.permission;

        // Verifica si el usuario tiene al menos uno de los permisos permitidos
        const hasPermission = allowedPermissions.some(permission => permissions.includes(permission));

        if (hasPermission) {
            next();
        } else {
            res.status(403).json({ msg: 'No tienes permisos para acceder a esta ruta' });
        }
    };
}

module.exports = checkPermissions;

