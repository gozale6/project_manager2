const supertest = require('supertest');

const app = require('../app');

let token;
beforeAll((done) => {
    supertest(app)
        .post("/login")
        .send({ "email": "admin@gmail.com", "password": "admin" })
        .expect(200)
        .end((err, res) => {
            if (err) return done(err);
            token = res.body.obj;
            done();
        });
});

describe('Probar sistema de proyectos', () => {

    it('Debería mostrar la lista de proyectos', (done) => {
        supertest(app)
            .get('/projects')
            .set('Authorization', `Bearer ${token}`)
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería no poder mostrar la lista de proyectos', (done) => {
        supertest(app)
            .get('/projects')
            .send()
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería mostrar el proyecto con cierto id', (done) => {
        supertest(app)
            .get('/projects/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería no mostrar el proyecto con cierto id', (done) => {
        supertest(app)
            .get('/projects/655fe4ff80afbf540deb88b1')
            .send()
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería agregar un proyecto correctamente', (done) => {
        supertest(app)
            .post('/projects')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": "Project ABC",
                "fecSol": "2023-01-01",
                "fecAr": "2023-12-31",
                "desc": "Description of Project ABC",
                "scrumMaster": "655fe4ff80afbf540deb88b1",
                "productOwner": "655fe4ff80afbf540deb88b2",
                "devTeam": ["655fe4ff80afbf540deb88b3", "655fe4ff80afbf540deb88b4"]
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería no poder agregar un proyecto', (done) => {
        supertest(app)
            .post('/projects')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": {},
                "fecSol": "2023-01-01",
                "fecAr": "2023-12-31",
                "desc": "Description of Project ABC",
                "scrumMaster": "655fe4ff80afbf540deb88b1",
                "productOwner": "655fe4ff80afbf540deb88b2",
                "devTeam": ["655fe4ff80afbf540deb88b3", "655fe4ff80afbf540deb88b4"]
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería modificar un proyecto', (done) => {
        supertest(app)
            .put('/projects/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": "Project XYZ",
                "fecSol": "2023-02-01",
                "fecAr": "2023-12-31",
                "desc": "Updated description",
                "scrumMaster": "655fe4ff80afbf540deb88b1",
                "productOwner": "655fe4ff80afbf540deb88b2",
                "devTeam": ["655fe4ff80afbf540deb88b3", "655fe4ff80afbf540deb88b4"]
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder reemplazar un proyecto', (done) => {
        supertest(app)
            .put('/projects/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": {},
                "fecSol": "2023-02-01",
                "fecAr": "2023-12-31",
                "desc": "Updated description",
                "scrumMaster": "655fe4ff80afbf540deb88b1",
                "productOwner": "655fe4ff80afbf540deb88b2",
                "devTeam": ["655fe4ff80afbf540deb88b3", "655fe4ff80afbf540deb88b4"]
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería poder modificar un proyecto', (done) => {
        supertest(app)
            .patch('/projects/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({ "name": "Project XYZ", "desc": null })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder modificar un proyecto', (done) => {
        supertest(app)
            .patch('/projects/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({ "name": {}, "desc": {} })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería poder eliminar un proyecto', (done) => {
        supertest(app)
            .delete('/projects/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder eliminar un proyecto', (done) => {
        supertest(app)
            .delete('/projects/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

});
