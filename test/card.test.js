const supertest = require('supertest');
const app = require('../app'); // Ajusta la ruta al archivo principal de tu aplicación

let token;
beforeAll((done) => {
    supertest(app)
        .post("/login")
        .send({ "email": "admin@gmail.com", "password": "admin" })
        .expect(200)
        .end((err, res) => {
            if (err) return done(err);
            token = res.body.obj;
            done();
        });
});

describe('Probar sistema de tarjetas', () => {

    it('Debería mostrar la lista de tarjetas', (done) => {
        supertest(app)
            .get('/cards')
            .set('Authorization', `Bearer ${token}`)
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder mostrar la lista de tarjetas', (done) => {
        supertest(app)
            .get('/cards')
            .send()
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería mostrar la tarjeta con cierto ID', (done) => {
        supertest(app)
            .get('/cards/655fe4ff80afbf540deb88b1') // Reemplaza con un ID válido de una tarjeta existente
            .set('Authorization', `Bearer ${token}`)
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería mostrar la tarjeta con cierto ID', (done) => {
        supertest(app)
            .get('/cards/655fe4ff80afbf540deb88b1') // Reemplaza con un ID válido de una tarjeta no existente
            .send()
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería agregar una tarjeta correctamente', (done) => {
        supertest(app)
            .post('/cards')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": "abcd",
                "desc": "dcba",
                "valorF": 100,
                "state": "En progreso",
                "responsable": "ID_del_responsable",
                "fechaCreacion": "2023-11-30T12:00:00.000Z"
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder agregar una tarjeta', (done) => {
        supertest(app)
            .post('/cards')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": {},
                "desc": {},
                "valorF": null,
                "state": "En progreso",
                "responsable": "ID_del_responsable",
                "fechaCreacion": "2023-11-30T12:00:00.000Z"
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería modificar una tarjeta', (done) => {
        supertest(app)
            .put('/cards/655fe4ff80afbf540deb88b1') // Reemplaza con un ID válido de una tarjeta existente
            .set('Authorization', `Bearer ${token}`)
            .send({ "name": "test", "desc": "test" })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder reemplazar una tarjeta', (done) => {
        supertest(app)
            .put('/cards/655fe4ff80afbf540deb88b1') // Reemplaza con un ID válido de una tarjeta existente
            .set('Authorization', `Bearer ${token}`)
            .send({ "name": {}, "desc": {} })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería poder modificar una tarjeta', (done) => {
        supertest(app)
            .patch('/cards/655fe4ff80afbf540deb88b1') // Reemplaza con un ID válido de una tarjeta existente
            .set('Authorization', `Bearer ${token}`)
            .send({ "name": "test", "desc": null })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder modificar una tarjeta', (done) => {
        supertest(app)
            .patch('/cards/655fe4ff80afbf            .540deb88b1') // Reemplaza con un ID válido de una tarjeta existente
            .set('Authorization', `Bearer ${token}`)
            .send({ "name": {}, "desc": {} })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería poder eliminar una tarjeta', (done) => {
        supertest(app)
            .delete('/cards/655fe4ff80afbf540deb88b1') // Reemplaza con un ID válido de una tarjeta existente
            .set('Authorization', `Bearer ${token}`)
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder eliminar una tarjeta', (done) => {
        supertest(app)
            .delete('/cards/655fe4ff80afbf540deb88b1') // Reemplaza con un ID válido de una tarjeta no existente
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

});

