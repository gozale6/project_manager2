const supertest = require('supertest');
const app = require('../app'); // Ajusta la ruta al archivo principal de tu aplicación

let token;
beforeAll((done) => {
    supertest(app)
        .post("/login")
        .send({ "email": "admin@gmail.com", "password": "admin" })
        .expect(200)
        .end((err, res) => {
            if (err) return done(err);
            token = res.body.obj;
            done();
        });
});

describe('Pruebas para socials', ()=>{
    it('Debería crear una red social', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .post('/socials')
            .send({
                "name": "LinkedIn",
                "url": "https://www.linkedin.com/"
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder crear una red social', (done) => {
        supertest(app)
            .post('/socials')
            .send({
                "name": {},
                "url": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería listar redes sociales', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .get('/socials')
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería mostrar una red social con cierto id', (done) => {
        supertest(app)
            .get('/socials/655fe4ff80afbf540deb88b1')
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería mostrar una red social con cierto id', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .get('/socials/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería reemplazar una red social', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .put('/socials/655fe4ff80afbf540deb88b1')
            .send({
                "name": "LinkedIn",
                "url": "https://www.linkedin.com/"
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder reemplazar una red social', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .put('/socials/655fe4ff80afbf540deb88b1')
            .send({
                "name": {},
                "url": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería modificar una red social', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .patch('/socials/655fe4ff80afbf540deb88b1')
            .send({
                "name": "LinkedIn",
                "url": null
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder modificar una red social', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .patch('/socials/655fe4ff80afbf540deb88b1')
            .send({
                "name": {},
                "url": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería poder eliminar una red social', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .delete('/socials/655fe4ff80afbf540deb88b1')
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder eliminar una red social', (done) => {
        supertest(app)
            .delete('/socials/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
});