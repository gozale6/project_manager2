const supertest = require('supertest');

const app = require('../app');

describe('Probar el sistema de autenticacion', ()=>{
    it ('Deberia de obtener un login con un user y un pass ok', (done)=>{
        supertest(app).post('/login')
        .send({"email": "user@gmail.com","password": "user"})
        .expect(200)
        .end(function(error, res){
            if(error){
                done(error)
            }else{
                done(); //la funcion done en limpio declara que la prueba fue exitosa en su totalidad
            }
        });
    });

    it('Deberia de rechazar un login con user y pass incorrectos', (done)=>{
        supertest(app).post('/login')
        .send({"email": "abc@gmail.com", "password": "abcd"})
        .expect(403)
        .end(function(err, res){
            if(err){
                done(err)
            }else{
                done();
            }
        })
    })

});