const supertest = require('supertest');
const app = require('../app'); // Ajusta la ruta al archivo principal de tu aplicación

let token;
beforeAll((done) => {
    supertest(app)
        .post("/login")
        .send({ "email": "admin@gmail.com", "password": "admin" })
        .expect(200)
        .end((err, res) => {
            if (err) return done(err);
            token = res.body.obj;
            done();
        });
});


describe('Probar sistema de tablas Scrum', () => {

    it('Debería mostrar la lista de tablas Scrum', (done) => {
        supertest(app)
            .get('/scrum-tables')
            .set('Authorization', `Bearer ${token}`)
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería no poder mostrar la lista de tablas Scrum', (done) => {
        supertest(app)
            .get('/scrum-tables')
            .send()
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería mostrar la tabla Scrum con cierto id', (done) => {
        supertest(app)
            .get('/scrum-tables/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería no mostrar la tabla Scrum con cierto id', (done) => {
        supertest(app)
            .get('/scrum-tables/655fe4ff80afbf540deb88b1')
            .send()
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería agregar una tabla Scrum correctamente', (done) => {
        supertest(app)
            .post('/scrum-tables')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "productBacklog": ["feature1", "feature2"],
                "releaseBacklog": ["release1", "release2"],
                "sprintBacklog": ["sprint1", "sprint2"]
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería no poder agregar una tabla Scrum', (done) => {
        supertest(app)
            .post('/scrum-tables')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "productBacklog": {},
                "releaseBacklog": {},
                "sprintBacklog": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería modificar una tabla Scrum', (done) => {
        supertest(app)
            .put('/scrum-tables/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "productBacklog": ["feature1", "feature2"],
                "releaseBacklog": ["release1", "release2"],
                "sprintBacklog": ["sprint1", "sprint2"]
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder reemplazar una tabla Scrum', (done) => {
        supertest(app)
            .put('/scrum-tables/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "productBacklog": {},
                "releaseBacklog": {},
                "sprintBacklog": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería poder modificar una tabla Scrum', (done) => {
        supertest(app)
            .patch('/scrum-tables/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "productBacklog": ["feature1", "feature2"],
                "releaseBacklog": null
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder modificar una tabla Scrum', (done) => {
        supertest(app)
            .patch('/scrum-tables/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "productBacklog": {},
                "releaseBacklog": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería poder eliminar una tabla Scrum', (done) => {
        supertest(app)
            .delete('/scrum-tables/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .expect(200)
            .end(function (err, res)
            {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder eliminar una tabla Scrum', (done) => {
        supertest(app)
            .delete('/scrum-tables/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

});
