const supertest = require('supertest');
const app = require('../app'); // Ajusta la ruta al archivo principal de tu aplicación

let token;
beforeAll((done) => {
    supertest(app)
        .post("/login")
        .send({ "email": "admin@gmail.com", "password": "admin" })
        .expect(200)
        .end((err, res) => {
            if (err) return done(err);
            token = res.body.obj;
            done();
        });
});

describe('Pruebas para users',()=>{
    it('Debería crear un usuario', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .post('/users')
            .send({
                "name": "John",
                "lastName": "Doe",
                "email": "john.doe@example.com",
                "password": "password123",
                "permission": "admin"
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder crear un usuario', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .post('/users')
            .send({
                "name": {},
                "lastName": {},
                "email": {},
                "password": {},
                "permission": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería listar usuarios', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .get('/users')
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería mostrar un usuario con cierto id', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .get('/users/655fe4ff80afbf540deb88b1')
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería mostrar un usuario con cierto id', (done) => {
        supertest(app)
            .get('/users/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería reemplazar un usuario', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .put('/users/655fe4ff80afbf540deb88b1')
            .send({
                "name": "John",
                "lastName": "Doe",
                "email": "john.doe@example.com",
                "password": "password123"
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder reemplazar un usuario', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .put('/users/655fe4ff80afbf540deb88b1')
            .send({
                "name": {},
                "lastName": {},
                "email": {},
                "password": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería modificar un usuario', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .patch('/users/655fe4ff80afbf540deb88b1')
            .send({
                "name": "John",
                "lastName": "Doe",
                "email": "john.doe@example.com",
                "password": null
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder modificar un usuario', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .patch('/users/655fe4ff80afbf540deb88b1')
            .send({
                "name": {},
                "lastName": {},
                "email": {},
                "password": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería poder eliminar un usuario', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .delete('/users/655fe4ff80afbf540deb88b1')
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder eliminar un usuario', (done) => {
        supertest(app)
            .delete('/users/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
});