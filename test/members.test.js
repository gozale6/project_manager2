const supertest = require('supertest');
const app = require('../app'); 

let token;
beforeAll((done) => {
    supertest(app)
        .post("/login")
        .send({ "email": "admin@gmail.com", "password": "admin" })
        .expect(200)
        .end((err, res) => {
            if (err) return done(err);
            token = res.body.obj;
            done();
        });
});

describe('Probar sistema de miembros', () => {

    it('Debería mostrar la lista de miembros', (done) => {
        supertest(app)
            .get('/members')
            .set('Authorization', `Bearer ${token}`)
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder mostrar la lista de miembros', (done) => {
        supertest(app)
            .get('/members')
            .send()
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería mostrar el miembro con cierto id', (done) => {
        supertest(app)
            .get('/members/655fe4ff80afbf540deb88b1') 
            .set('Authorization', `Bearer ${token}`)
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería mostrar el miembro con cierto id', (done) => {
        supertest(app)
            .get('/members/655fe4ff80afbf540deb88b1') 
            .send()
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería agregar un miembro correctamente', (done) => {
        supertest(app)
            .post('/members')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": "John",
                "fecNac": "1990-01-01",
                "address": "123 Main St",
                "curp": "ABCD123456EFGHJKL",
                "rfc": "RFC123456ABC",
                "social": "LinkedIn",
                "roles": ["developer"],
                "skills": ["JavaScript", "Node.js"]
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err)
                else
                    done();
            });
    });

    it('No debería poder agregar un miembro', (done) => {
        supertest(app)
            .post('/members')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": {},
                "fecNac": "1990-01-01",
                "address": "123 Main St",
                "curp": "ABCD123456EFGHJKL",
                "rfc": "RFC123456ABC",
                "social": "LinkedIn",
                "roles": ["developer"],
                "skills": ["JavaScript", "Node.js"]
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err)
                else
                    done();
            });
    });

    it('Debería modificar un miembro', (done) => {
        supertest(app)
            .put('/members/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": "John",
                "fecNac": "1990-01-01",
                "address": "123 Main St",
                "curp": "ABCD123456EFGHJKL",
                "rfc": "RFC123456ABC",
                "social": "LinkedIn",
                "roles": ["developer"],
                "skills": ["JavaScript", "Node.js"]
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err)
                else
                    done();
            });
    });

    it('No debería poder reemplazar un miembro', (done) => {
        supertest(app)
            .put('/members/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": {},
                "fecNac": "1990-01-01",
                "address": "123 Main St",
                "curp": "ABCD123456EFGHJKL",
                "rfc": "RFC123456ABC",
                "social": "LinkedIn",
                "roles": ["developer"],
                "skills": ["JavaScript", "Node.js"]
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err)
                else
                    done();
            });
    });

    it('Debería poder modificar un miembro', (done) => {
        supertest(app)
            .patch('/members/655fe4ff80afbf540deb88b1') 
            .set('Authorization', `Bearer ${token}`)
            .send({ "name": "John", "skills": null })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err)
                else
                    done();
            });
    });

    // ...

    it('No debería poder modificar un miembro', (done) => {
        supertest(app)
            .patch('/members/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({ "name": {}, "skills": {} })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err)
                else
                    done();
            });
    });

    it('Debería poder eliminar un miembro', (done) => {
        supertest(app)
            .delete('/members/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err)
                else
                    done();
            });
    });

    it('No debería poder eliminar un miembro', (done) => {
        supertest(app)
            .delete('/members/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err)
                else
                    done();
            });
    });

});

