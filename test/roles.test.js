const supertest = require('supertest');
const app = require('../app'); // Ajusta la ruta al archivo principal de tu aplicación

let token;
beforeAll((done) => {
    supertest(app)
        .post("/login")
        .send({ "email": "admin@gmail.com", "password": "admin" })
        .expect(200)
        .end((err, res) => {
            if (err) return done(err);
            token = res.body.obj;
            done();
        });
});

describe('Probar sistema de roles', () => {

    it('Debería mostrar la lista de roles', (done) => {
        supertest(app)
            .get('/roles')
            .set('Authorization', `Bearer ${token}`)
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería no poder mostrar la lista de roles', (done) => {
        supertest(app)
            .get('/roles')
            .send()
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería mostrar el rol con cierto id', (done) => {
        supertest(app)
            .get('/roles/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería no mostrar el rol con cierto id', (done) => {
        supertest(app)
            .get('/roles/655fe4ff80afbf540deb88b1')
            .send()
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería agregar un rol correctamente', (done) => {
        supertest(app)
            .post('/roles')
            .set('Authorization', `Bearer ${token}`)
            .send({ "rol": "admin", "permissons": ["read", "write"] })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería no poder agregar un rol', (done) => {
        supertest(app)
            .post('/roles')
            .set('Authorization', `Bearer ${token}`)
            .send({ "rol": {}, "permissons": {} })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería modificar un rol', (done) => {
        supertest(app)
            .put('/roles/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "rol": "admin",
                "permissons": ["read", "write"]
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder reemplazar un rol', (done) => {
        supertest(app)
            .put('/roles/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "rol": {},
                "permissons": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería poder modificar un rol', (done) => {
        supertest(app)
            .patch('/roles/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({ "rol": "admin", "permissons": null })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder modificar un rol', (done) => {
        supertest(app)
            .patch('/roles/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({ "rol": {}, "permissons": {} })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería poder eliminar un rol', (done) => {
        supertest(app)
            .delete('/roles/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder eliminar un rol', (done) => {
        supertest(app)
            .delete('/roles/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

});
