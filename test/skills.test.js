const supertest = require('supertest');
const app = require('../app'); // Ajusta la ruta al archivo principal de tu aplicación

let token;
beforeAll((done) => {
    supertest(app)
        .post("/login")
        .send({ "email": "admin@gmail.com", "password": "admin" })
        .expect(200)
        .end((err, res) => {
            if (err) return done(err);
            token = res.body.obj;
            done();
        });
});


describe('Pruebas para skills',()=>{
    it('Debería crear una habilidad', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .post('/skills')
            .send({
                "name": "JavaScript",
                "lvl": "Intermediate"
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder crear una habilidad', (done) => {
        supertest(app)
            .post('/skills')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": {},
                "lvl": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería listar habilidades', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .get('/skills')
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería mostrar una habilidad con cierto id', (done) => {
        supertest(app)
            .get('/skills/655fe4ff80afbf540deb88b1')
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería mostrar una habilidad con cierto id', (done) => {
        supertest(app)
            .get('/skills/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería reemplazar una habilidad', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .put('/skills/655fe4ff80afbf540deb88b1')
            .send({
                "name": "JavaScript",
                "lvl": "Advanced"
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder reemplazar una habilidad', (done) => {
        supertest(app)
            .put('/skills/655fe4ff80afbf540deb88b1')
            .set('Authorization', `Bearer ${token}`)
            .send({
                "name": {},
                "lvl": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería modificar una habilidad', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .patch('/skills/655fe4ff80afbf540deb88b1')
            .send({
                "name": "JavaScript",
                "lvl": null
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder modificar una habilidad', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .patch('/skills/655fe4ff80afbf540deb88b1')
            .send({
                "name": {},
                "lvl": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('Debería poder eliminar una habilidad', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .delete('/skills/655fe4ff80afbf540deb88b1')
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

    it('No debería poder eliminar una habilidad', (done) => {
        supertest(app)
            .delete('/skills/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });

});
