const supertest = require('supertest');
const app = require('../app'); // Ajusta la ruta al archivo principal de tu aplicación

let token;
beforeAll((done) => {
    supertest(app)
        .post("/login")
        .send({ "email": "admin@gmail.com", "password": "admin" })
        .expect(200)
        .end((err, res) => {
            if (err) return done(err);
            token = res.body.obj;
            done();
        });
});

describe('Pruebas para teams',()=>{
    it('Debería crear un equipo', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .post('/teams')
            .send({
                "projectId": "655fe4ff80afbf540deb88b1",
                "team": ["John Doe", "Jane Doe"]
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder crear un equipo', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .post('/teams')
            .send({
                "projectId": {},
                "team": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería listar equipos', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .get('/teams')
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería mostrar un equipo con cierto id', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .get('/teams/655fe4ff80afbf540deb88b1')
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería mostrar un equipo con cierto id', (done) => {
        supertest(app)
            .get('/teams/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería reemplazar un equipo', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .put('/teams/655fe4ff80afbf540deb88b1')
            .send({
                "projectId": "655fe4ff80afbf540deb88b1",
                "team": ["John Doe", "Jane Doe"]
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder reemplazar un equipo', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .put('/teams/655fe4ff80afbf540deb88b1')
            .send({
                "projectId": {},
                "team": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería modificar un equipo', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .patch('/teams/655fe4ff80afbf540deb88b1')
            .send({
                "projectId": "655fe4ff80afbf540deb88b1",
                "team": null
            })
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder modificar un equipo', (done) => {
        supertest(app),
        .set('Authorization', `Bearer ${token}`)
            .patch('/teams/655fe4ff80afbf540deb88b1')
            .send({
                "projectId": {},
                "team": {}
            })
            .expect(500)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('Debería poder eliminar un equipo', (done) => {
        supertest(app)
        .set('Authorization', `Bearer ${token}`)
            .delete('/teams/655fe4ff80afbf540deb88b1')
            .expect(200)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
    it('No debería poder eliminar un equipo', (done) => {
        supertest(app)
            .delete('/teams/655fe4ff80afbf540deb88b1')
            .expect(401)
            .end(function (err, res) {
                if (err)
                    done(err);
                else
                    done();
            });
    });
    
});