const ScrumTable = require('../models/scrum_table');

function create(req, res, next) {
    const { productBacklog, releaseBacklog, sprintBacklog } = req.body;

    let newScrumTable = new ScrumTable({
        _productBacklog: productBacklog,
        _releaseBacklog: releaseBacklog,
        _sprintBacklog: sprintBacklog
    });

    newScrumTable.save().then(obj => res.status(200).json({
        msg: res.__('scrum_table.list.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('scrum_table.list.notok'),
        obj: ex
    }));
}

function list(req, res, next) {
    ScrumTable.find().then(objs => res.status(200).json({
        msg: res.__('scrum_table.list.ok'),
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: res.__('scrum_table.list.notok'),
        obj: ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    ScrumTable.findOne({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('scrum_table.index.ok') + ` ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('scrum_table.index.notok'),
        obj: ex
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const { productBacklog, releaseBacklog, sprintBacklog } = req.body;

    const updatedScrumTable = {
        _productBacklog: productBacklog,
        _releaseBacklog: releaseBacklog,
        _sprintBacklog: sprintBacklog
    };

    ScrumTable.findOneAndUpdate({"_id": id}, updatedScrumTable, {new: true}).then(obj => res.status(200).json({
        msg: res.__('scrum_table.replace.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('scrum_table.replace.notok'),
        obj: ex
    }));
}

function update(req, res, next) {
    const id = req.params.id;
    const { productBacklog, releaseBacklog, sprintBacklog } = req.body;

    const updatedScrumTable = {};
    if (productBacklog) updatedScrumTable._productBacklog = productBacklog;
    if (releaseBacklog) updatedScrumTable._releaseBacklog = releaseBacklog;
    if (sprintBacklog) updatedScrumTable._sprintBacklog = sprintBacklog;

    ScrumTable.findOneAndUpdate({"_id": id}, updatedScrumTable, {new: true}).then(obj => res.status(200).json({
        msg: res.__('scrum_table.update.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('scrum_table.update.notok'),
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    ScrumTable.findByIdAndDelete({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('scrum_table.destroy.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('scrum_table.destroy.notok'),
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, update, destroy
};
