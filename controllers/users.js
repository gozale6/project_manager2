const User = require ('../models/user');
const bcrypt = require('bcrypt');

async function create(req, res,next){
    const name = req.body.name;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;
    const permission = req.body.permission;
    
                        // recibe el numero de iteraciones que queremos hacer sobre una cadena
    const salt = await bcrypt.genSalt(10);
                                            // la cadena original y el saltKey
    const passwordHash = await bcrypt.hash(password, salt);

    let user = new User();

    if (permission === 'admin') {
        user._name = name;
        user._lastName = lastName;
        user._email = email;
        user._password = passwordHash;
        user._salt = salt;
        user._permission = ['read', 'write', 'delete'];
    } else if (permission === 'user') {
        user._name = name;
        user._lastName = lastName;
        user._email = email;
        user._password = passwordHash;
        user._salt = salt;
        user._permission = ['read'];
    }


    user.save().then(obj => res.status(200).json({
        msg: res.__('user.create.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('user.create.notok'),
        obj: ex
    }));
}

function list(req, res, next) {
    User.find().then(objs => res.status(200).json({
        msg: res.__('user.list.ok'),
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: res.__('user.list.notok'),
        obj: ex
    }));
}

function index(req, res,next){
    const id = req.params.id;
    User.findOne({"_id":id}).then(obj => res.status(200).json({
        msg: res.__('user.index.ok') + ` ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('user.index.notok'),
        obj: ex
    }));
}

function replace(req, res,next){
    const id = req.params.id;
    const name = req.body.name ? req.body.name : "";
    const lastName = req.body.lastName ? req.body.lastName : "";
    const email = req.body.email ? req.body.email : "";
    const password = req.body.password ? req.body.password : "";
    
    const user = new Object({
        _name: name,
        _lastName: lastName,
        _email: email,
        _password: password
    });

    User.findOneAndUpdate({"_id":id}, user, {new: true}).then(obj => res.status(200).json({
        msg: res.__('user.replace.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('user.replace.notok'),
        obj: ex
    }));
}

function update(req, res,next){
    const id = req.params.id;
    const name = req.body.name;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;

    const user = new Object();
    if(name) user._name = name;
    if(lastName) user._lastName = lastName;
    if(email) user._email = email;
    if(password) user._password = password;

    User.findOneAndUpdate({"_id":id}, user).then(obj => res.status(200).json({
        msg: res.__('user.update.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('user.update.notok'),
        obj: ex
    }));
}

function destroy(req, res,next){
    const id = req.params.id;
    User.findByIdAndRemove({"_id":id}).then(obj => res.status(200).json({
        msg: res.__('user.destroy.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('user.destroy.notok'),
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, update, destroy
};