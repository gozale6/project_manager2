const Skill = require('../models/skills');

function create(req, res, next) {
    const { name, lvl } = req.body;

    let newSkill = new Skill({
        _name: name,
        _lvl: lvl
    });

    newSkill.save().then(obj => res.status(200).json({
        msg: res.__('skill.create.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('skill.create.notok'),
        obj: ex
    }));
}

function list(req, res, next) {
    Skill.find().then(objs => res.status(200).json({
        msg: res.__('skill.list.ok'),
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: res.__('skill.list.notok'),
        obj: ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Skill.findOne({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('skill.index.ok') + ` ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('skill.index.notok'),
        obj: ex
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const { name, lvl } = req.body;

    const updatedSkill = {
        _name: name,
        _lvl: lvl
    };

    Skill.findOneAndUpdate({"_id": id}, updatedSkill, {new: true}).then(obj => res.status(200).json({
        msg: res.__('skill.replace.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('skill.replace.notok'),
        obj: ex
    }));
}

function update(req, res, next) {
    const id = req.params.id;
    const { name, lvl } = req.body;

    const updatedSkill = {};
    if (name) updatedSkill._name = name;
    if (lvl) updatedSkill._lvl = lvl;

    Skill.findOneAndUpdate({"_id": id}, updatedSkill, {new: true}).then(obj => res.status(200).json({
        msg: res.__('skill.update.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('skill.update.notok'),
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Skill.findByIdAndDelete({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('skill.destroy.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('skill.destroy.notok'),
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, update, destroy
};
