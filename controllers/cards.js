const Card = require('../models/card');

function create(req, res, next) {
    const { name, desc, valorF, state, responsable, fechaCreacion } = req.body;

    let newCard = new Card({
        _name: name,
        _desc: desc,
        _valorF: valorF,
        _state: state,
        _responsable: responsable,
        _fechaCreacion: fechaCreacion,
        _fechaModificacion: null
    });

    newCard.save().then(obj => res.status(200).json({
        msg: res.__('card.create.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('card.create.notok'),
        obj: ex
    }));
}

function list(req, res, next) {
    Card.find().then(objs => res.status(200).json({
        msg: res.__('card.list.ok'),
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: res.__('card.list.notok'),
        obj: ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Card.findOne({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('card.index.ok') + ` ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('card.index.notok'),
        obj: ex
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const { name, desc, valorF, state, responsable, fechaCreacion } = req.body;

    const updatedCard = new Object({
        name: name,
        desc: desc,
        valorF: valorF,
        state: state,
        responsable: responsable,
        fechaCreacion: fechaCreacion,
        fechaModificacion: null
    });

    Card.findOneAndUpdate({"_id": id}, updatedCard, {new: true}).then(obj => res.status(200).json({
        msg: res.__('card.replace.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('card.replace.notok'),
        obj: ex
    }));
}

function update(req, res, next) {
    const id = req.params.id;
    const { name, desc, valorF, state, responsable, fechaCreacion, fechaModificacion } = req.body;

    const updatedCard = new Object();
    if (name) updatedCard.name = name;
    if (desc) updatedCard.desc = desc;
    if (valorF) updatedCard.valorF = valorF;
    if (state) updatedCard.state = state;
    if (responsable) updatedCard.responsable = responsable;
    if (fechaCreacion) updatedCard.fechaCreacion = fechaCreacion;
    if (fechaModificacion) updatedCard.fechaModificacion = fechaModificacion;

    Card.findOneAndUpdate({"_id": id}, updatedCard).then(obj => res.status(200).json({
        msg: res.__('card.update.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('card.update.notok'),
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Card.findByIdAndDelete({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('card.destroy.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('card.destroy.notok'),
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, update, destroy
};
