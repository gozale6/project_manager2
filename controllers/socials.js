const Social = require('../models/social');

function create(req, res,next){
    const name = req.body.name;
    const url = req.body.url;

    let social = new Social({
        name: name,
        url: url    });

    social.save().then(obj => res.status(200).json({
        msg: res.__('social.create.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('social.create.notok'),
        obj: ex
    }));
}

function list(req, res, next) {
    Social.find().then(objs => res.status(200).json({
        msg: res.__('social.list.ok'),
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: res.__('social.list.ok'),
        obj: ex
    }));
}

function index(req, res,next){
    const id = req.params.id;
    Social.findOne({"_id":id}).then(obj => res.status(200).json({
        msg: res.__('social.index.ok') + ` ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('social.index.ok'),
        obj: ex
    }));
}

function replace(req, res,next){
    const id = req.params.id;
    const name = req.body.name ? req.body.name : "";
    const url = req.body.url ? req.body.url : "";
    
    const social = new Object({
        name: name,
        url: url
    });

    Social.findOneAndUpdate({"_id":id}, social, {new: true}).then(obj => res.status(200).json({
        msg: res.__('social.replace.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('social.replace.ok'),
        obj: ex
    }));
}

function update(req, res,next){
    const id = req.params.id;
    const name = req.body.name;
    const url = req.body.url;

    const social = new Object();
    if(name) social._name = name;
    if(url) social._url = url;

    Social.findOneAndUpdate({"_id":id}, social).then(obj => res.status(200).json({
        msg: res.__('social.update.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('social.update.ok'),
        obj: ex
    }));

}

function destroy(req, res,next){
    const id = req.params.id;
    Social.findByIdAndDelete({"_id":id}).then(obj => res.status(200).json({
        msg: res.__('social.destroy.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('social.destroy.ok'),
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, update, destroy
};