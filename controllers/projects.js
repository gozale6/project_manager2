const Project = require('../models/project');

function create(req, res, next) {
    const { name, fecSol, fecAr, desc, scrumMaster, productOwner, devTeam } = req.body;

    let newProject = new Project({
        _name: name,
        _fecSol: fecSol,
        _fecAr: fecAr,
        _desc: desc,
        _scrumMaster: scrumMaster,
        _productOwner: productOwner,
        _devTeam: devTeam
    });

    newProject.save().then(obj => res.status(200).json({
        msg: res.__('project.create.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('project.create.notok'),
        obj: ex
    }));
}

function list(req, res, next) {
    Project.find().populate('_scrumMaster _productOwner _devTeam').then(objs => res.status(200).json({
        msg: res.__('project.list.ok'),
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: res.__('project.list.notok'),
        obj: ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Project.findOne({"_id": id}).populate('_scrumMaster _productOwner _devTeam').then(obj => res.status(200).json({
        msg: res.__('project.index.ok') + ` ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('project.index.notok'),
        obj: ex
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const { name, fecSol, fecAr, desc, scrumMaster, productOwner, devTeam } = req.body;

    const updatedProject = {
        _name: name,
        _fecSol: fecSol,
        _fecAr: fecAr,
        _desc: desc,
        _scrumMaster: scrumMaster,
        _productOwner: productOwner,
        _devTeam: devTeam
    };

    Project.findOneAndUpdate({"_id": id}, updatedProject, {new: true}).populate('_scrumMaster _productOwner _devTeam').then(obj => res.status(200).json({
        msg: res.__('project.replace.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('project.replace.notok'),
        obj: ex
    }));
}

function update(req, res, next) {
    const id = req.params.id;
    const { name, fecSol, fecAr, desc, scrumMaster, productOwner, devTeam } = req.body;

    const updatedProject = {};
    if (name) updatedProject._name = name;
    if (fecSol) updatedProject._fecSol = fecSol;
    if (fecAr) updatedProject._fecAr = fecAr;
    if (desc) updatedProject._desc = desc;
    if (scrumMaster) updatedProject._scrumMaster = scrumMaster;
    if (productOwner) updatedProject._productOwner = productOwner;
    if (devTeam) updatedProject._devTeam = devTeam;

    Project.findOneAndUpdate({"_id": id}, updatedProject, {new: true}).populate('_scrumMaster _productOwner _devTeam').then(obj => res.status(200).json({
        msg: res.__('project.update.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('project.update.notok'),
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Project.findByIdAndDelete({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('project.destroy.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('project.destroy.notok'),
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, update, destroy
};
