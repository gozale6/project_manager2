const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('config');

function home(req, res, next) {
    res.render('index', { title: 'Express' });
}

function login(req, res, next) {
    const email = req.body.email;
    const password = req.body.password;
    User.findOne({"_email": email}).then(user => {
        const jwtKey = config.get('secret.key');
        if(user){
            bcrypt.hash(password,user.salt, (err,hash)=>{
                if(err){
                    res.status(403).json({
                        msg: res.__('login.wrong'),
                        obj: err
                    });
                }else{
                    if(hash === user.password){
                        res.status(200).json({
                            msg: res.__('login.ok'),
                            // pide un objeto con la info que vamos a meter en el jwt, y el jwtkey
                            obj: jwt.sign({data: user.data, exp: Math.floor(Date.now()/1000)+3600, permission: user.permission},jwtKey)
                        });
                    }else{
                        res.status(403).json({
                            msg: res.__('login.wrong'),
                            obj: null
                        });
                    }
                }
            })
        }else{
            res.status(403).json({
                msg: res.__('login.wrong'),
                obj: null
            });
        }
    }).catch(ex => res.status(403).json({
        msg: res.__('login.wrong'),
        obj: ex
    }));

}

module.exports = {home, login}