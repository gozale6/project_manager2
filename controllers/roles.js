const Rol = require('../models/role');

function create(req, res, next) {
    const { rol, permissons } = req.body;

    let newRol = new Rol({
        _rol: rol,
        _permissons: permissons
    });

    newRol.save().then(obj => res.status(200).json({
        msg: res.__('rol.create.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('rol.create.notok'),
        obj: ex
    }));
}

function list(req, res, next) {
    Rol.find().then(objs => res.status(200).json({
        msg: res.__('rol.list.ok'),
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: res.__('rol.list.notok'),
        obj: ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Rol.findOne({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('rol.index.ok') + ` ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('rol.index.notok'),
        obj: ex
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const { rol, permissons } = req.body;

    const updatedRol = {
        _rol: rol,
        _permissons: permissons
    };

    Rol.findOneAndUpdate({"_id": id}, updatedRol, {new: true}).then(obj => res.status(200).json({
        msg: res.__('rol.replace.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('rol.replace.notok'),
        obj: ex
    }));
}

function update(req, res, next) {
    const id = req.params.id;
    const { rol, permissons } = req.body;

    const updatedRol = {};
    if (rol) updatedRol._rol = rol;
    if (permissons) updatedRol._permissons = permissons;

    Rol.findOneAndUpdate({"_id": id}, updatedRol, {new: true}).then(obj => res.status(200).json({
        msg: res.__('rol.update.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('rol.update.notok'),
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Rol.findByIdAndDelete({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('rol.destroy.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('rol.destroy.ok'),
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, update, destroy
};
