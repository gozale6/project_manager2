const Team = require('../models/team');

function create(req, res, next) {
    const projectId = req.body.projectId;
    const team = req.body.team;

    let newTeam = new Team({
        _projectId: projectId,
        _team: team
    });

    newTeam.save().then(obj => res.status(200).json({
        msg: res.__('team.create.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('team.create.notok'),
        obj: ex
    }));
}

function list(req, res, next) {
    Team.find().then(objs => res.status(200).json({
        msg: res.__('team.list.ok'),
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: res.__('team.list.notok'),
        obj: ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Team.findOne({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('team.index.ok') + ` ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('team.index.notok'),
        obj: ex
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const projectId = req.body.projectId ? req.body.projectId : "";
    const team = req.body.team ? req.body.team : "";

    const updatedTeam = new Object({
        _projectId: projectId,
        _team: team
    });

    Team.findOneAndUpdate({"_id": id}, updatedTeam, {new: true}).then(obj => res.status(200).json({
        msg: res.__('team.replace.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('team.replace.notok'),
        obj: ex
    }));
}

function update(req, res, next) {
    const id = req.params.id;
    const projectId = req.body.projectId;
    const team = req.body.team;

    const updatedTeam = new Object();
    if (projectId) updatedTeam._projectId = projectId;
    if (team) updatedTeam._team = team;

    Team.findOneAndUpdate({"_id": id}, updatedTeam).then(obj => res.status(200).json({
        msg: res.__('team.update.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('team.update.notok'),
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Team.findByIdAndDelete({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('team.destroy.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('team.destroy.ok'),
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, update, destroy
};
