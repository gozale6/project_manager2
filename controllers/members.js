const Member = require('../models/member');


function create(req, res, next) {
    const { name, fecNac, address, curp, rfc, social, roles, skills } = req.body;

    let newMember = new Member({
        _name: name,
        _fecNac: fecNac,
        _address: address,
        _curp: curp,
        _rfc: rfc,
        _social: social,
        _roles: roles,
        _skills: skills

    });

    newMember.save().then(obj => res.status(200).json({
        msg: res.__('member.create.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('member.create.notok'),
        obj: ex
    }));
}

function list(req, res, next) {
    Member.find().then(objs => res.status(200).json({
        msg: res.__('member.list.ok'),
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: res.__('member.list.notok'),
        obj: ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Member.findOne({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('member.index.ok') + ` ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('member.index.notok'),
        obj: ex
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const { name, fecNac, address, curp, rfc, social, roles, skills} = req.body;

    const updatedMember = {
        _name: name,
        _fecNac: fecNac,
        _address: address,
        _curp: curp,
        _rfc: rfc,
        _social: social,
        _roles: roles,
        _skills: skills
    };

    Member.findOneAndUpdate({"_id": id}, updatedMember, {new: true}).then(obj => res.status(200).json({
        msg: res.__('member.replace.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('member.replace.notok'),
        obj: ex
    }));
}

function update(req, res, next) {
    const id = req.params.id;
    const { name, fecNac, address, curp, rfc, social, roles, skills } = req.body;

    const updatedMember = {};
    if (name) updatedMember._name = name;
    if (fecNac) updatedMember._fecNac = fecNac;
    if (address) updatedMember._address = address;
    if (curp) updatedMember._curp = curp;
    if (rfc) updatedMember._rfc = rfc;
    if (social) updatedMember._social = social;
    if (roles) updatedMember._roles = roles;
    if (skills) updatedMember._skills = skills;

    Member.findOneAndUpdate({"_id": id}, updatedMember, {new: true}).then(obj => res.status(200).json({
        msg: res.__('member.update.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('member.update.notok'),
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Member.findByIdAndDelete({"_id": id}).then(obj => res.status(200).json({
        msg: res.__('member.destroy.ok'),
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: res.__('member.destroy.notok'),
        obj: ex
    }));
}

module.exports = {
    list, index, create, replace, update, destroy
};
