## project_manager 

**Instalación**
-
Lo primero que se debe hacer es clonar el proyecto escribiendo en la terminal:
>git clone git@gitlab.com:gozale6/project_manager2.git
>
Despues de clonarlo es necesario tener instalado node.js y nmp:
>sudo apt install nodejs npm

Por ultimo se debe instalar express (en la carpeta del proyecto):
>npm init -y
npm install express

**Introducción**
-
Project_manager nace de la necesidad de una empresa de llevar de mejor manera el control de sus proyectos. Por lo que aqui se crea project_manager, utilizando de guia todos los requerimientos del cliente.

**Autores**
-
    -Mauricio Ramirez Ruiz 
    -Ana Marcela Sigala Cavazos
    -Alejandro Arturo Gonzalez Flores
    -Ramón Reyna Garcia 

**Imagen Docker**
-
Para descargar la imagen de docker del proyecto haga [click aqui](https://hub.docker.com/r/lagmau2/project_manager/tags)
o corra en su terminal:
>docker pull lagmau2/project_manager:version1

Recuerda que debes tener instalado docker para ejecutar y descargar imagenes. Para ver como se instala has [click aqui](https://www.docker.com/get-started)

**Diagramas de interacción**
-
#### Diagrama de Inicio de Sesión

Esta documentación describe en detalle la secuencia de acciones para el proceso de inicio de sesión en el sistema utilizando una red social. Este flujo proporciona una vista completa de cómo un usuario interactúa con el sistema y la red social para autenticarse y acceder a su cuenta.

Descripción de la Secuencia Usuario selecciona una Red Social: La secuencia comienza cuando el usuario decide iniciar sesión en el sistema utilizando una red social. El usuario elige la red social de su elección, como Facebook o Google.

Solicitud de Autorización para Acceder a Datos: Después de seleccionar la red social, el sistema envía una solicitud al servicio de la red social para obtener autorización para acceder a los datos del usuario. Esta solicitud se realiza para que el sistema pueda autenticar al usuario a través de la red social.

Redirección a la Página de Inicio de Sesión de la Red Social: La red social recibe la solicitud de autorización y redirige al usuario a la página de inicio de sesión de la red social. Aquí, el usuario debe proporcionar sus credenciales de inicio de sesión en la red social (por ejemplo, correo electrónico y contraseña).

Proporciona Credenciales de Inicio de Sesión: El usuario introduce sus credenciales de inicio de sesión (correo electrónico y contraseña) en la página de inicio de sesión de la red social.

Autenticación de Credenciales: La red social recibe las credenciales del usuario y procede a autenticar al usuario. Verifica si las credenciales son válidas y pertenecen a una cuenta existente.

Validación de Credenciales: Una vez que la red social autentica al usuario, valida las credenciales con el sistema. Esto asegura que el usuario tenga una cuenta asociada en el sistema.

Autorización de Acceso: Después de la validación exitosa de las credenciales, la red social autoriza el acceso al sistema en nombre del usuario. Esto significa que el sistema tiene permiso para acceder a ciertos datos del usuario en la red social.

Generación de Token de Acceso: La red social genera un token de acceso seguro y lo envía de vuelta al sistema. Este token de acceso actúa como una credencial de autenticación válida que el sistema utilizará para identificar al usuario en sesiones futuras.

Redirección a Página de Inicio de Sesión Exitoso: La red social redirige al usuario de vuelta al sistema, indicando que la autenticación y autorización fueron exitosas. El usuario ahora está autenticado en el sistema y puede acceder a su cuenta.

Recibe Token de Acceso: El sistema recibe el token de acceso generado por la red social. Este token se utiliza para autenticar al usuario en el sistema en sesiones posteriores.

Esta secuencia detallada garantiza que el usuario pueda utilizar las credenciales de la red social para acceder al sistema de manera segura y eficiente.

![Diagrama de inicio de sesión](https://gitlab.com/gozale6/project_manager2/-/raw/main/diagramas%20de%20secuencia/Inicio_de_sesi%C3%B3n_1.png?ref_type=heads)

#### Diagrama de Administración de Proyecto

Esta sección detallada describe el flujo completo de acciones involucradas en la gestión de proyectos dentro de nuestro sistema. La secuencia "Administrar Proyecto" abarca desde la visualización de proyectos hasta la eliminación de proyectos, incluyendo la gestión de detalles y la asignación de equipos de desarrollo.

Descripción de la Secuencia Inicia Sesión: El proceso comienza cuando un usuario inicia sesión en el sistema con sus credenciales.

Visualiza Proyectos: Una vez autenticado, el usuario puede ver una lista de proyectos disponibles en el sistema.

Selecciona Proyecto: El usuario selecciona el proyecto específico que desea administrar.

Visualiza Detalles del Proyecto: El sistema muestra los detalles del proyecto seleccionado, incluyendo su nombre, fecha de solicitud, fecha de inicio, descripción, y los roles clave como Product Owner y Scrum Master.

Modifica Detalles del Proyecto: El usuario tiene la opción de modificar los detalles del proyecto, como su descripción o fechas importantes.

Actualiza Detalles del Proyecto: Una vez que el usuario realiza modificaciones, el sistema actualiza la información del proyecto con los nuevos datos.

Gestiona Equipo de Desarrollo: El usuario puede gestionar el equipo de desarrollo asignado al proyecto.

Agrega Miembro al Equipo: El usuario puede agregar miembros al equipo de desarrollo, especificando sus habilidades y roles.

Asigna Roles y Responsabilidades: Para cada miembro del equipo, el usuario puede asignar roles y responsabilidades específicas dentro del proyecto.

Modifica Equipo de Desarrollo: Si es necesario, el usuario puede realizar modificaciones en el equipo, como cambiar roles o agregar nuevos miembros.

Modifica Miembro del Equipo: Se permite la modificación de información específica de los miembros del equipo, como sus habilidades o roles dentro del proyecto.

Actualiza Habilidades y Detalles del Miembro: Cuando se realizan cambios en la información de los miembros, el sistema actualiza los datos correspondientes.

Elimina Miembro del Equipo: El usuario puede eliminar a un miembro del equipo si ya no está involucrado en el proyecto.

Elimina Proyecto: En cualquier momento, el usuario tiene la opción de eliminar el proyecto por completo.

Elimina Proyecto del Sistema: Cuando se elimina un proyecto, el sistema borra todos los datos relacionados con ese proyecto de manera permanente.

Redirige a Proyectos: Finalmente, el usuario es redirigido de nuevo a la lista de proyectos disponibles para continuar administrando otros proyectos o realizar otras acciones.

Esta secuencia proporciona una visión completa de cómo los usuarios pueden administrar proyectos en el sistema, desde la visualización inicial hasta la modificación y eliminación de proyectos, así como la gestión detallada del equipo de desarrollo. Estos pasos aseguran un control completo y eficiente de la gestión de proyectos en nuestro sistema.

![Administración_de_Proyecto_1.png](https://gitlab.com/gozale6/project_manager2/-/raw/main/diagramas%20de%20secuencia/Administraci%C3%B3n_de_Proyecto_1.png?ref_type=heads)

#### Diagrama de Administración de Miembros

Esta sección detallada describe el flujo completo de acciones involucradas en la gestión de miembros del equipo dentro de nuestro sistema. La secuencia "Administrar Miembro del Equipo" abarca desde la visualización de miembros hasta la eliminación de miembros, incluyendo la gestión de detalles y habilidades.

Descripción de la Secuencia Inicia Sesión: El proceso comienza cuando un usuario inicia sesión en el sistema con sus credenciales.

Visualiza Miembros del Equipo: Una vez autenticado, el usuario puede ver una lista de miembros del equipo disponibles en el sistema.

Selecciona Miembro del Equipo: El usuario selecciona un miembro específico del equipo que desea administrar.

Visualiza Detalles del Miembro: El sistema muestra los detalles del miembro seleccionado, incluyendo su nombre completo, fecha de nacimiento, CURP, RFC, domicilio y una lista de habilidades.

Modifica Detalles del Miembro: El usuario tiene la opción de modificar los detalles del miembro, como la dirección o la lista de habilidades.

Actualiza Detalles del Miembro: Una vez que el usuario realiza modificaciones, el sistema actualiza la información del miembro con los nuevos datos.

Visualiza Habilidades del Miembro: El usuario puede ver las habilidades actuales del miembro, que están ranqueadas por nivel (junior, senior, master).

Agrega Habilidad al Miembro: El usuario puede agregar nuevas habilidades al miembro, especificando el nivel de habilidad (junior, senior, master) para cada una.

Modifica Habilidad del Miembro: Se permite la modificación de las habilidades existentes del miembro, incluyendo el cambio de nivel de habilidad.

Actualiza Habilidades del Miembro: Cuando se realizan cambios en las habilidades del miembro, el sistema actualiza los datos correspondientes.

Elimina Habilidad del Miembro: El usuario puede eliminar habilidades específicas del miembro si ya no son relevantes.

Elimina Miembro del Equipo: En cualquier momento, el usuario tiene la opción de eliminar al miembro del equipo si ya no está involucrado en el proyecto o en el equipo.

Elimina Miembro del Sistema: Cuando se elimina un miembro del equipo, el sistema borra todos los datos relacionados con ese miembro de manera permanente.

Redirige a Miembros del Equipo: Finalmente, el usuario es redirigido de nuevo a la lista de miembros del equipo disponibles para continuar administrando otros miembros o realizar otras acciones.

Esta secuencia proporciona una visión completa de cómo los usuarios pueden administrar miembros del equipo en el sistema, desde la visualización inicial hasta la modificación y eliminación de miembros, así como la gestión detallada de habilidades. Estos pasos aseguran un control completo y eficiente de la gestión de miembros del equipo en nuestro sistema.

![administración_de_miembros.png](https://gitlab.com/gozale6/project_manager2/-/raw/main/diagramas%20de%20secuencia/administraci%C3%B3n_de_miembros.png?ref_type=heads)

**Diagrama de clases**
-
Este diagrama de clases representa la estructura y las relaciones entre las clases en un sistema de gestión de proyectos de software basado en la metodología Scrum. El sistema se centra en la gestión de usuarios, habilidades, roles y permisos en proyectos de desarrollo de software.

1. Usuario (User)
   - La clase ‘Usuario’ representa a los usuarios del sistema, que pueden estar involucrados en proyectos de desarrollo de software.
   - Tiene los atributos ‘idUsuario’,’nombre’, ‘fechaNacimiento’, ‘domicilio’, ‘CURP’, ‘RFC’, ‘redesSociales’ (que es una lista de instancias de la clase RedesSociales), ‘roles’ (que es una lista de instancias de la clase Rol) y ‘habilidades’ (que es una lista de instancias de la clase Habilidad)
   - Puede iniciar sesión en el sistema a través de varias redes sociales.
   - Puede ser asignado a diferentes roles en proyectos.

2. Redes Sociales
   - La clase ‘Redes Sociales’ representa las redes sociales asociadas a un usuario.
   - Tiene atributos como ‘nombre’ y ‘url’ para almacenar información sobre las redes sociales de un usuario.

3. Habilidad
   - La clase ‘Habilidad’ representa las habilidades relacionadas con el desarrollo de software que los usuarios pueden poseer.
   - Tiene atributos como ‘nombre’ y ‘nivel’ para describir una habilidad específica y su nivel que es un tipo de datos enumerado llamado ‘NivelHabilidad’ que son: junior, senior, master.

4. Equipo
   - La clase ‘Equipo’ está relacionada con los proyectos y los roles de los usuarios en esos proyectos.
   - Tiene atributos ‘usuario’, ‘proyecto’, y ‘rol’, que asocian a los usuarios con los proyectos y sus roles respectivos en esos proyectos.

5. Proyecto
   - La clase ‘Proyecto’ representa un proyecto de desarrollo de software.
   - Tiene atributos como ‘idProyecto’, ‘nombreProyecto’, ‘fechaSolicitud’, ‘fechaArranque’, ‘descripcion', ‘scrumMaster’, ‘productOwner’, y ‘equipoDesarrollo’, que almacenan detalles del proyecto y las personas involucradas.

6. Rol
   - La clase ‘Rol’ representa los roles que los usuarios pueden desempeñar en proyectos.
   - Tiene atributos ‘rol’ y ‘permisos’ que permiten definir el rol y los permisos asociados a ese rol, el atributo rol es un tipo de datos enumerado llamado ‘Roles’ (SCRUM Master, ProductOwner,Team), y permisos es una lista de un tipo de datos llamado ‘permiso’ que solo tiene la descripción del sistema
   
7. Tarjeta
Almacena información sobre tareas o historias de usuario en un proyecto de desarrollo de software. Cada tarjeta tiene un identificador único (idTarjeta), un nombre, una descripción detallada, un valor asignado según la secuencia Fibonacci, un estado que refleja su progreso, un responsable designado, y registros de fechas de creación y modificación.

8. TableroScrum: Esta tabla incluye campos como un identificador único (idTablero), nombre del tablero, descripción, fechas de creación y modificación, y también contiene las secciones "productBacklog," "releaseBacklog," y "sprintBacklog". Estas secciones permiten organizar y priorizar las tarjetas o historias de usuario dentro del contexto de Scrum, lo que facilita la planificación y seguimiento de las actividades a lo largo del proyecto.

![Diagrama de clases](https://gitlab.com/gozale6/project_manager2/-/raw/main/assets/Diagrama_de_clases.png?ref_type=heads)

Novedades
Se implementaron los siguientes componentes y características:

Componentes de seguridad, incluyendo JWT y almacenamiento seguro de datos.
Manejo de ambientes para adaptarse a diferentes entornos.
ACL para controlar el acceso basado en roles.
Internacionalización (i18n) y localización (L10n) para admitir múltiples idiomas. (Usando las cookies 'en','es','en-US' y 'es-MX', dejando como default el uso del lenguaje inglés)
Pruebas
Las pruebas fueron construidas utilizando JEST y Super Test para cada ruta del proyecto, cumpliendo con la regla del 50%.