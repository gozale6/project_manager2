const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _rol: {type: String, enum:['SCRUM_MASTER','ProductOwner','Team']},
    _permissons: [{type: String, enum: ['GET','POST','DELETE','PATH','PUT']}]
});

class Rol{
    constructor(rol, permissons){
        this._rol = rol;
        this.permissons = permissons;
    }

    get rol(){
        return this._rol;
    }

    set rol(v){
        this._rol = v;
    }

    get permissons(){
        return this._permissons;
    }

    set permissons(v){
        this._permissons = v;
    }
}

schema.loadClass(Rol);

module.exports = mongoose.model('Rol',schema);