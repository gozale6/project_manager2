const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _projectId: String,
    _team:[{
        user: {type: mongoose.Schema.ObjectId, ref: 'Member'},
        rol: {type: mongoose.Schema.ObjectId, ref: 'Rol'}
    }]
});

class Team{
    constructor(projectId, team){
        this._projectId = projectId;
        this._team = team;
    }

    get projectId(){
        return this._projectId;
    }

    set projectId(v){
        this._projectId = v;
    }

    get team(){
        return this._team;
    }

    set team(v){
        this._team = v;
    }

}

schema.loadClass(Team);

module.exports = mongoose.model('Team',schema);