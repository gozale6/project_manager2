const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _name: String,
    _url: String
});

class Social{
    constructor(name, url){
        this._name = name;
        this._url = url;
    }

    get name(){
        return this._name;
    }

    set name(v){
        this._name = v;
    }

    get url(){
        return this._url;
    }

    set url(v){
        this._url = v;
    }
}

schema.loadClass(Social);

module.exports = mongoose.model('Social',schema);