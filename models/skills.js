const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _name: String,
    _lvl: {type: String, enum:['JUNIOR','SENIOR','MASTER']}
});

class Skill{
    constructor(name, lvl){
        this._name = name;
        this._lvl = lvl;
    }

    get name(){
        return this._name;
    }

    set name(v){
        this._name = v;
    }

    get lvl(){
        return this.l_vl;
    }

    set lvl(v){
        this._lvl = v;
    }
}

schema.loadClass(Skill);

module.exports = mongoose.model('Skill',schema);