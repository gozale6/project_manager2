const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _productBacklog: [{type: mongoose.Schema.ObjectId, ref: 'Card'}],
    _releaseBacklog: [{type: mongoose.Schema.ObjectId, ref: 'Card'}],
    _sprintBacklog: [{type: mongoose.Schema.ObjectId, ref: 'Card'}]
});

class Scrum_table{
    constructor(product, release, sprint){
        this._productBakclog = product;
        this._releaseBacklog = release,
        this._sprintBacklog = sprint;
    }

    get productBacklog(){
        return this._productBacklog;
    }

    set productBacklog(v){
        this._productBacklog = v;
    }

    get releaseBacklog(){
        return this._releaseBacklog;
    }

    set releaseBacklog(v){
        this._releaseBacklog = v;
    }

    get sprintBacklog(){
        return this._sprintBacklog;
    }

    set sprintBacklog(v){
        this._sprintBacklog = v;
    }

}

schema.loadClass(Scrum_table);

module.exports = mongoose.model('Scrum_table',schema);