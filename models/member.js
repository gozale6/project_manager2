const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _name: String,
    _fecNac: Date,
    _address: {
        street: String,
        number: String,
        zip: Number,
        city: String,
        state: String,
        country: String
    },
    _curp: String,
    _rfc: String,
    _social: {type: mongoose.Schema.ObjectId, ref: 'Social'},
    _roles: {type: mongoose.Schema.ObjectId, ref: 'Role'},
    _skills: {type: mongoose.Schema.ObjectId, ref: 'Skill'}


});

class Member {
    constructor(name, fecNac, address, curp, rfc, social, roles, skills){
        this._name = name;
        this._fecNac = fecNac;
        this._address = address;
        this._curp = curp;
        this._rfc = rfc;
        this._social = social;
        this._roles = roles;
        this._skills = skills;
    }

    get name(){
        return this._name;
    }

    set name(v){
        this._name = v;
    }

    get fecNac(){
        return this._fecNac;
    }

    set fecNac(v){
        this._fecNac = v;
    }

    get address(){
        return this._address;
    }

    set address(v){
        this._address = v;
    }

    get curp(){
        return this._curp;
    }

    set curp(v){
        this._curp = v;
    }

    get rfc(){
        return this._rfc;
    }

    set rfc(v){
        this._rfc = v;
    }

    get social(){
        return this._social;
    }

    set social(v){
        this._social = v;
    }

    get roles(){
        return this._roles;
    }

    set roles(v){
        this._roles = v;
    }

    get skills(){
        return this._skills;
    }

    set skills(v){
        this._skills = v;
    }

}

schema.loadClass(Member);
module.exports = mongoose.model('Member',schema);