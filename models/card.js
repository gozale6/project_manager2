const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _name: String,
    _desc: String,
    _valorF: Number,
    _state: String,
    _reponsable: {type: mongoose.Schema.ObjectId, ref: 'Member'},
    _fechaCreacion: Date,
    _fechaModificacion: Date

});

class Card {
    constructor(name, desc, valorF, state, responsable, fechaCreacion) {
        this._name = name;
        this._desc = desc;
        this._valorF = valorF;
        this._state = state;
        this._responsable = responsable;
        this._fechaCreacion = fechaCreacion;
        this._fechaModificacion = null;
    }

    get name() {
        return this._name;
    }

    set name(v) {
        this._name = v;
    }

    get desc() {
        return this._desc;
    }

    set desc(v) {
        this._desc = v;
    }

    get valorF() {
        return this._valorF;
    }

    set valorF(v) {
        this._valorF = v;
    }

    get state() {
        return this._state;
    }

    set state(v) {
        this._state = v;
    }

    get responsable() {
        return this._responsable;
    }

    set responsable(v) {
        this._responsable = v;
    }

    get fechaCreacion() {
        return this._fechaCreacion;
    }

    set fechaCreacion(v) {
        this._fechaCreacion = v;
    }

    get fechaModificacion() {
        return this._fechaModificacion;
    }

    set fechaModificacion(v) {
        this._fechaModificacion = v;
    }
}


schema.loadClass(Card);

module.exports = mongoose.model('Card',schema);