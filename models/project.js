const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _name: String,
    _fecSol: Date,
    _fecAr: Date,
    _desc: String,
    _scrumMaster: {type: mongoose.Schema.ObjectId, ref: 'Member'},
    _productOwner: {type: mongoose.Schema.ObjectId, ref: 'Member'},
    _devTeam: {type: mongoose.Schema.ObjectId, ref: 'Team'}
});

class Project{
    constructor(name, fecSol, fecAr, desc, scrumMaster, productOwner, devTeam){
        this._name = name;
        this._fecSol = fecSol;
        this._fecAr = fecAr;
        this._desc = desc;
        this._scrumMaster = scrumMaster;
        this._productOwner = productOwner;
        this._devTeam = devTeam;

    }

    get name(){
        return this._name;
    }

    set name(v){
        this._name = v;
    }

    get fecSol(){
        return this._fecSol;
    }

    set fecSol(v){
        this._fecSol = v;
    }

    get fecAr(){
        return this._fecAr;
    }

    set fecAr(v){
        this._fecAr = v;
    }

    get desc(){
        return this._desc;
    }

    set desc(v){
        this._desc = v;
    }

    get scrumMaster(){
        return this._scrumMaster;
    }

    set scrumMaster(v){
        this._scrumMaster = v;
    }

    get productOwner(){
        return this._productOwner;
    }

    set productOwner(v){
        this._productOwner = v;
    }

    get devTeam(){
        return this._devTeam;
    }

    set devTeam(v){
        this._devTeam = v;
    }

}

schema.loadClass(Project);

module.exports = mongoose.model('Project',schema);